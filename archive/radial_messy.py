# Import libraries

import time
import cv2
import numpy as np
#import cProfile

#import matplotlib.pyplot as plt

#pr = cProfile.Profile()

all_timestamps = []

for i in range(10):
    timestamps = {}

    timestamps['start'] = time.time()

    # Load image

    image = cv2.imread('samples/gray_carpet_straight.jpg')
    #image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    timestamps['load'] = time.time()

    # We are skipping the following steps: undistort, color balance, warp

    # HLS Thresholding: 

    #hls = cv2.cvtColor(image, cv2.COLOR_RGB2HLS)

    # We can optimize this step by directly converting the original image
    # from BGR to HLS instead of taking two separate steps to do it.

    hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
    # h_channel = hls[:, :, 0]
    # l_channel = hls[:, :, 1]
    # s_channel = hls[:, :, 2]

    # Can we simplify the above step?
    h_channel, _, s_channel = cv2.split(hls)

    timestamps['hls'] = time.time()

    h_thresh = (20, 50)
    s_thresh = (100, 255)

    # def threshold(img, thresh):
    #     binary = np.zeros_like(img)
    #     binary[(img > thresh[0]) & (img <= thresh[1])] = 255
    #     return binary

    def threshold(img, thresh):
        # TODO: It is recommended to move this outside the main loop
        binary = np.zeros_like(img)
        np.where((img > thresh[0]) & (img <= thresh[1]), 255, binary)
        return binary

    #pr.enable()

    h_binary = threshold(h_channel, h_thresh)
    s_binary = threshold(s_channel, s_thresh)

    binary = cv2.bitwise_and(h_binary, s_binary)

    #pr.disable()

    timestamps['threshold'] = time.time()

    # print('Summary of times with Radial Search method:\n')
    # print('{:<10s} {:>10s}'.format('Process', 'Time (ms)'))
    # print('-' * 25)

    # last = timestamps['start']

    # for key in timestamps:
    #     if key == 'start':
    #         continue
    #     time_ms = 1000*(timestamps[key] - last)
    #     print('{:<10s} {:>10.3f}'.format(key, time_ms))
    #     last = timestamps[key]

    # print('-' * 25)

    # total_time_ms = 1000*(last - timestamps['start'])
    # print('{:<10s} {:>10.3f}'.format('Total', total_time_ms))

    # print('=' * 25)

    all_timestamps.append(timestamps)

#pr.print_stats()


# Plot all the timestamps in a graph

import matplotlib.pyplot as plt

# Create lists to hold the times for each process
load_times = []
hls_times = []
threshold_times = []
total_times = []

# Calculate the total time for each process for each run
for timestamps in all_timestamps:
    last = timestamps['start']
    for key in timestamps:
        if key == 'start':
            continue
        time_ms = 1000*(timestamps[key] - last)  # convert to ms
        if key == 'load':
            load_times.append(time_ms)
        elif key == 'hls':
            hls_times.append(time_ms)
        elif key == 'threshold':
            threshold_times.append(time_ms)
        last = timestamps[key]
    total_times.append(1000*(last - timestamps['start']))  # convert to ms

# Calculate the average time for each process for the last 5 runs
avg_load_time = sum(load_times[-5:]) / 5
avg_hls_time = sum(hls_times[-5:]) / 5
avg_threshold_time = sum(threshold_times[-5:]) / 5
avg_total_time = sum(total_times[-5:]) / 5

# Print the averages
print('Average load time for last 5 runs: {:.3f} ms'.format(avg_load_time))
print('Average hls time for last 5 runs: {:.3f} ms'.format(avg_hls_time))
print('Average threshold time for last 5 runs: {:.3f} ms'.format(avg_threshold_time))
print('Average total time for last 5 runs: {:.3f} ms'.format(avg_total_time))

# Plot the timings
# labels = ['Run {}'.format(i) for i in range(len(all_timestamps))]
# x = np.arange(len(labels))  # the label locations
# width = 0.2  # the width of the bars

# fig, ax = plt.subplots()
# rects1 = ax.bar(x - 3*width/2, load_times, width, label='load')
# rects2 = ax.bar(x - width/2, hls_times, width, label='hls')
# rects3 = ax.bar(x + width/2, threshold_times, width, label='threshold')
# rects4 = ax.bar(x + 3*width/2, total_times, width, label='Total')

# # Add some text for labels, title and custom x-axis tick labels, etc.
# ax.set_ylabel('Time (ms)')
# ax.set_title('Time for Each Run')
# ax.set_xticks(x)
# ax.set_xticklabels(labels)
# ax.legend()

# fig.tight_layout()

# plt.show()
