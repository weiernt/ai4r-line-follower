import cv2
import streamlit as st
import time

# Set two column page layout

st.set_page_config(layout='wide')
left_column, right_column = st.columns(2)


# Function to set video capture properties
def set_capture_properties(capture, resolution, frame_rate):
    width, height = get_resolution(resolution)
    capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    capture.set(cv2.CAP_PROP_FPS, frame_rate)

# Function to get resolution tuple from selected resolution
def get_resolution(resolution):
    resolutions = {
        "480p": (640, 480),
        "540p": (960, 540),
        "720p": (1280, 720),
        "1080p": (1920, 1080)
    }
    return resolutions[resolution]

res_list = ["480p", "540p", "720p", "1080p"]

frame_rates = {
    "480p": (5, 10, 15, 20, 30),
    "540p": (5, 10, 15),
    "720p": (5, 10),
    "1080p": (5,)
}

def main():
    # Parameter selection in the left column:
    with left_column:
        st.header("Settings")
        st.subheader("Thresholding Parameters")
        st.write("Which channels would you like to select for thresholding?")

        hls_select = st.multiselect(
            "Select HLS Channels",
            ["Hue", "Lightness", "Saturation"],
            default = ["Saturation"]
        )

        st.write("If you select multiple channels, a bitwise AND operation will be performed on the thresholded images.")

        if hls_select:
            for channel in hls_select:
                if channel == "Hue":
                    h_range = st.slider(
                        "Select a threshold range for the Hue channel", 0, 255, (20, 50)
                    )
                elif channel == "Lightness":
                    l_range = st.slider(
                        "Select a threshold range for the Lightness channel", 0, 255, (150, 255)
                    )
                elif channel == "Saturation":
                    s_range = st.slider(
                        "Select a threshold range for the Saturation channel", 0, 255, (100, 255)
                    )
        
        if len(hls_select) == 0:
            st.warning("Please select at least one channel for thresholding")
        
        st.subheader("Image Processing Operations")
        st.write("Select all that apply -")
        morph = st.checkbox("Morphological operations", False)
        if morph:
            st.write("Select the morphological operations you would like to perform")

            # Erosion Operation
            erosion = st.checkbox("Erosion", False)
            if erosion:
                kernel_erode = st.slider(
                    "Select the kernel size for erosion",
                    min_value = 3,
                    max_value = 21, 
                    step = 2,
                    value = 3
                )

            # Dilation Operation
            dilation = st.checkbox("Dilation")
            if dilation:
                kernel_dilate = st.slider(
                    "Select the kernel size for dilation",
                    min_value = 3,
                    max_value = 21, 
                    step = 2,
                    value = 3
                )
            
            # Opening Operation
            opening = st.checkbox("Opening")
            if opening:
                kernel_open = st.slider(
                    "Select the kernel size for opening",
                    min_value = 3,
                    max_value = 21, 
                    step = 2,
                    value = 3
                )
            
            # Closing Operation
            closing = st.checkbox("Closing")
            if closing:
                kernel_close = st.slider(
                    "Select the kernel size for closing",
                    min_value = 3,
                    max_value = 21, 
                    step = 2,
                    value = 3
                )

            st.markdown("""---""")      
        
        largcon = st.checkbox("Get largest contour", False)
        
        blur = st.checkbox("Gaussian Blur", False)

        if blur:
            #blur = True
            kernel_blur = st.slider("Select the kernel size for Gaussian Blur",
                                    min_value = 3,
                                    max_value = 21,
                                    step = 2,
                                    value = 3
                                )
        
        st.subheader("Lane Detection")

        lane_detection = st.radio("Which lane detection method would you like to use?"
                    , ["Manual", "BN Search"], index = 1)

        st.write("**Search parameters:**")

        if lane_detection == "Manual":
            thinning = st.checkbox("Thinning operation", True)
            step_size = st.slider("Select the **step size** for manual search",
                                    min_value = 10, 
                                    max_value = 100, 
                                    step = 10,
                                    value = 50
                                )
                    
        if lane_detection == "BN Search":
            shape = st.radio("Which shape would you like to use for search?",
                                ("Circle", "Ellipse"), index = 0)

            st.write("Please select the search parameters for the search")

            if shape == "Circle":
                radius = st.slider("Select the **radius** for Search",
                                    min_value = 10,
                                    max_value = 100,
                                    step = 10,
                                    value = 50
                                )
                start_angle, end_angle = st.slider("Select the **angle range** for Search arc",
                                                    min_value = 90,
                                                    max_value = 450,
                                                    step = 10,
                                                    value = (210, 330)
                                                )
            elif shape == "Ellipse":
                a = st.slider("Select the **major axis** length",
                                min_value = 20,
                                max_value = 200,
                                step = 10,
                                value = 100)
                b = st.slider("Select the **minor axis** length",
                                min_value = 10,
                                max_value = 100,
                                step = 10,
                                value = 50)
                start_angle, end_angle = st.slider("Select the **angle range** for Search arc",
                                                    min_value = 90,
                                                    max_value = 450,
                                                    step = 10,
                                                    value = (210, 330)
                                                )

    with right_column: 
        res = st.radio("Select resolution:", res_list, index=0, key="resolution", horizontal = True)
        # Frame rate selection
        frame_rate = st.radio("Select frame rate:", frame_rates[res], index=0, key="frame_rate", horizontal = True)
        # Create video capture object
        capture = cv2.VideoCapture(0)
        # Set capture properties
        set_capture_properties(capture, res, frame_rate)

        # Create placeholders for displaying real-time data
        frame_rate_placeholder = st.empty()
        resolution_placeholder = st.empty()
        frame_placeholder = st.empty()

        # Start capturing and displaying frames
        stop_button = st.button("Stop", key = 'stop')

    while not stop_button:
        ret, frame = capture.read()

        # Check if frame is not None before displaying
        if frame is not None:
            # Display frame
            frame_placeholder.image(frame, channels="BGR")

            # Display current frame rate
            current_frame_rate = capture.get(cv2.CAP_PROP_FPS)
            frame_rate_placeholder.text(f"Current frame rate: {current_frame_rate:.2f}")

            # Display current resolution
            current_width = capture.get(cv2.CAP_PROP_FRAME_WIDTH)
            current_height = capture.get(cv2.CAP_PROP_FRAME_HEIGHT)
            resolution_placeholder.text(f"Resolution: {current_width}x{current_height}")
        else:
            frame_placeholder.text("Waiting for the camera to initialize...")
            time.sleep(1)
            capture.release()
            capture = cv2.VideoCapture(0)
            set_capture_properties(capture, res, frame_rate)

    # Release the capture object
    capture.release()

if __name__ == "__main__":
    main()