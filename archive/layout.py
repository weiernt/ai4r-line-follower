from detector_lib import *

import os
import cv2
import time

from collections import deque

import streamlit as st
from PIL import Image

# Setting up paths for dummy image read
path = 'samples/streamcam/'
path += 'new_tape/'
file_list = os.listdir(path)


# Set page config
st.set_page_config(layout='wide')

# Create two columns
left_column, right_column = st.columns(2)

# Populate the left column
with left_column:
    st.header("Settings")

    st.subheader("Search Parameters")
    algo = st.radio("Algorithm", ('Circle', 'Ellipse'), index=0, horizontal=True)

    start_angle, end_angle = st.slider("Select search angle range (degrees)", 180, 360, (210, 330))

    if algo == "Circle":
        radius = st.slider("Select a radius for the circle:", 10, 100, 50)
    elif algo == "Ellipse":
        a = st.slider("Select a major axis for the ellipse:", 10, 100, 50)
        b = st.slider("Select a minor axis for the ellipse:", 5, 50, 25)


    st.subheader("Buffer size for FPS calculation")
    buffer_size = st.slider("Select a buffer size", 2, 100, 50)


timestamps = deque(maxlen = buffer_size)

# Populate the right column
with right_column:
    draw = st.checkbox("Visualize Output? (Slows down algorithm)", value = False)
    stop = st.button("Stop")
    #st.subheader("Search Parameters")
    #st.write("Radius: ", radius)
    #st.write("Angle Range: ", (start_angle, end_angle))
    if draw:
        st.header("Preview")
    image_placeholder = st.empty()
    st.subheader("Frame Statistics")
    ft_placeholder = st.empty()
    fps_placeholder = st.empty()

timestamps = deque(maxlen = buffer_size)
frametimes = deque(maxlen = buffer_size)

if algo == "Circle":
    ld = LineDetectorCircle(radius, start_angle = start_angle, end_angle = end_angle, draw = draw)
elif algo == "Ellipse":
    ld = LineDetectorEllipse(a, b, start_angle = start_angle, end_angle = end_angle, draw = draw)

while not stop:
    for file in file_list:
        image = cv2.imread(os.path.join(path, file))

        frame_start = time.time()
        hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
        _, _, s = cv2.split(hls)
        img = cv2.threshold(s, 100, 255, cv2.THRESH_BINARY)[1]
        points = ld(img)
        frame_end = time.time()

        frame_time = (frame_end - frame_start)*1000
        frametimes.append(frame_time)

        # Add current timestamp to the buffer
        timestamps.append(time.time())

        if draw:
            image = ld.img_color

        # Calculate the frame rate for the last N frames
        if len(timestamps) >= buffer_size:
            start_time = timestamps[0]
            end_time = timestamps[-1]
            fps = buffer_size/(end_time - start_time)
            frame_time_avg = sum(frametimes)/len(frametimes)
            if draw:
                # Overlay the frame rate on the image
                cv2.putText(image, f"FPS: {fps:.2f}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
                cv2.putText(image, f"Frame Time: {frame_time_avg:.2f} ms", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
            else:
                image_placeholder.write(f"FPS: {fps:.2f}")
            ft_placeholder.write(f"Frame Time - Current: {frame_time:.2f} ms \t Average: {frame_time_avg:.2f} ms")
            fps_placeholder.write(f"Algorithm FPS - Current: {1000/frame_time:.0f} \t Average: {1000/frame_time_avg:.0f}")

        if draw:
            image_placeholder.image(image, caption = 'Image', use_column_width = True)

# if stop:
#     ft_placeholder.write(f"Algorithm FPS (Average): {1000/frame_time_avg:.0f}")