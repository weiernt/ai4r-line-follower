import cv2

cap = cv2.VideoCapture(0)

# Set frame rate
cap.set(cv2.CAP_PROP_FPS, 10)  # Replace 30 with your desired frame rate

# Set exposure
cap.set(cv2.CAP_PROP_EXPOSURE, -4)  # Replace -4 with your desired exposure value

# Set shutter speed
cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 0.25)  # Disable auto exposure

# Check if the parameters were successfully set
print("Frame rate:", cap.get(cv2.CAP_PROP_FPS))
print("Exposure:", cap.get(cv2.CAP_PROP_EXPOSURE))

# Display frames

while True:
    ret, frame = cap.read()
    cv2.imshow('frame', frame)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break

# Release the video capture object and destroy any open windows
cap.release()
cv2.destroyAllWindows()