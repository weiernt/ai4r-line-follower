import streamlit as st
import cv2
import os
import time
import numpy as np
from collections import deque
import matplotlib.pyplot as plt

from detector_lib import LineDetectorEllipse

path = 'samples/streamcam/'
path += 'new_tape/'

file_list = os.listdir(path)

# Here is your main loop
def main():
    buffer_size = 50
    timestamps = deque(maxlen=buffer_size)

    placeholder = st.empty()

    while True:
        for file in file_list:
            image = cv2.imread(os.path.join(path, file))

            hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
            _, _, s = cv2.split(hls)
            img = cv2.threshold(s, 100, 255, cv2.THRESH_BINARY)[1]

            ld = LineDetectorEllipse(draw = True)
            points = ld(img)
            image = ld.img_color

            # Add current timestamp to the buffer
            timestamps.append(time.time())

            # Calculate the frame rate for the last 100 frames
            if len(timestamps) >= buffer_size:
                start_time = timestamps[0]
                end_time = timestamps[-1]
                elapsed_time = end_time - start_time
                fps = buffer_size / elapsed_time
                # Overlay the frame rate on the image
                cv2.putText(image, "FPS: {:.2f}".format(fps), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

            # Convert the image from BGR to RGB
            image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

            # Draw the image on Streamlit using st.image
            placeholder.image(image_rgb)

            # Sleep for a bit to slow down the loop
            time.sleep(0.1)

# Run the main function as a Streamlit app
if __name__ == "__main__":
    main()