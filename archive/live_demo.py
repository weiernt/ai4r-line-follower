import cv2
import streamlit as st

st.title("Webcam Live Stream")

# Create a VideoCapture object
cap = cv2.VideoCapture(0)

# Check if camera opened successfully
if not cap.isOpened():
    st.error("Unable to open the camera.")

# Set video width and height
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

# Create an empty placeholder to display the frame
placeholder = st.empty()

# Continuously read and display frames from the webcam
while True:
    # Read a frame from the video capture
    ret, frame = cap.read()

    # If frame reading is successful
    if ret:
        # Display the frame in Streamlit
        placeholder.image(frame, channels="BGR")
