# Library Imports
import cv2
from detector_lib import *

config = {
    "hls_select": ["Saturation"],
    "s_range": (50, 255),
    "morph": False,
    "erosion": False,
    "dilation": False,
    "kernel_erode": None,
    "kernel_dilate": None,
    "kernel_open": None,
    "kernel_close": None,
    "largcon": True,
    "blur": False,
    "kernel_blur": None,
    "shape": "Circle",
    "radius": 100,
    "start_angle": 180,
    "end_angle": 360,
}

class ImageProcessor:
    def __init__(self, config):
        self.get_params_from_config(config)
    
    def load_image(self, image):
        """Converts the image to HLS and returns the HLS channels"""
        hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
        self.h, self.l, self.s = cv2.split(hls)
    
    def get_params_from_config(self, config):
        """Extracts the params fromt he config dict"""
        self.hls_select = config.get("hls_select", [])
        self.h_range = config.get("h_range", None)
        self.l_range = config.get("l_range", None)
        self.s_range = config.get("s_range", None)
        self.morph = config.get("morph", False)
        self.erosion = config.get("erosion", False)
        self.dilation = config.get("dilation", False)
        self.opening = config.get("opening", False)
        self.closing = config.get("closing", False)
        self.kernel_erode = config.get("kernel_erode", None)
        self.kernel_dilate = config.get("kernel_dilate", None)
        self.kernel_open = config.get("kernel_open", None)
        self.kernel_close = config.get("kernel_close", None)
        self.largcon = config.get("largcon", False)
        self.blur = config.get("blur", False)
        self.kernel_blur = config.get("kernel_blur", None)
        self.shape = config.get("shape", None)
        self.radius = config.get("radius", None)
        self.start_angle = config.get("start_angle", None)
        self.end_angle = config.get("end_angle", None)
        self.a = config.get("a", None)
        self.b = config.get("b", None)
    
    def threshold_image(self):
        thresholded = []
        if not len(self.hls_select) == 0:
            for channel in self.hls_select:
                if channel == "Hue":
                    thresholded.append(thresh_image(self.h, self.h_range))
                elif channel == "Lightness":
                    thresholded.append(thresh_image(self.l, self.l_range))
                elif channel == "Saturation":
                    thresholded.append(thresh_image(self.s, self.s_range))

        self.im = combine_images(thresholded)
        return self.im

    def preprocess_image(self):
        """Applies pre-processing operations on the image such as - 
        - Morphological operations (optional)
        - Get Largest contour (optional)
        - Blur (optional)
        """
        # Morphological operations
        if self.morph:
            if self.erosion:
                self.im = erode(self.im, self.kernel_erode)
            if self.dilation:
                self.im = dilate(self.im, self.kernel_dilate)
            if self.opening:
                self.im = open_op(self.im, self.kernel_open)
            if self.closing:
                self.im = close_op(self.im, self.kernel_close)
        
        # Get largest contour
        if self.largcon:
            self.im = get_largest_contour(self.im)

        # Blur operation        
        if self.blur:
            self.im = gauss_blur(self.im, self.kernel_blur)

        return self.im

    def detect_line(self):
        """Detects the line using either of the specified methods"""
        if self.shape == "Circle":
            ld = LineDetectorCircle(self.radius, self.start_angle, self.end_angle)
        elif self.shape == "Ellipse":
            ld = LineDetectorEllipse(self.a, self.b, self.start_angle, self.end_angle)
        points = ld(self.im)
        return points
    
    def process(self, image):
        """Processes the image and returns the detected line"""
        self.load_image(image)
        self.threshold_image()
        self.preprocess_image()
        points = self.detect_line()
        return points

ip = ImageProcessor(config)

# Test detection class

# Go to the step2_test.py script to find out what I need to do

# Select folder
dir_path = "samples/streamcam/new_tape/"

# Iterate through folder
import os
file_list = os.listdir(dir_path)
for file in file_list:
    image = cv2.imread(os.path.join(dir_path, file))
    points = ip.process(image)
    print(points)
