from process import ImageProcessor, BNSearch
import streamlit as st
import cv2
import numpy as np
import pandas as pd
import toml

#import plotly_express as px

cfg = {
    'undistort': False,
    'color_balance': False,
    'warp': None,
    'threshold_channels': None,
    'h_thresh': None,
    'l_thresh': None,
    's_thresh': None,
    'largcon': False,
    'blur': False,
    'lane_detection': None,
    'shape': None,
    'draw': False,
    'radius': None,
    'angles': None,
    'ellipse_ab': None,
}

st.title("Image Processor")

# List of image paths
image_names = [
    "gray_carpet_straight",
    "gray_carpet_left",
    "gray_carpet_right",
    "gray_carpet_sharp_left",
    "gray_carpet_sharp_right",
    "gray_lab_straight",
    "hallway",
    "red_surface",
]

# Get user's selection
selected_image = st.selectbox("Select an image", image_names, index = 0)

image = cv2.imread('samples/{}.jpg'.format(selected_image))
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

st.subheader("Original Image")
st.image(image, use_column_width=True)

processor = ImageProcessor(image)

undistort = st.checkbox("Undistort Image", True)
cfg['undistort'] = undistort
if undistort:
    undistorted = processor.undistort(image)
    st.subheader("Undistorted Image")
    st.image(undistorted, use_column_width=True)

color_balance = st.checkbox("Color Balance")
cfg['color_balance'] = color_balance
if color_balance:
    st.subheader("Color Balanced Image")
    balanced = processor.color_balance()
    st.image(balanced, use_column_width=True)

warp = st.checkbox("Perspective Transformation", True)
if warp:
    p1 = tuple(st.text_input('Enter the co-ordinate for the top-left point', '0.28, 0.45').split(','))
    p2 = tuple(st.text_input('Enter the co-ordinate for the top-right point', '0.78,0.47').split(','))
    p3 = tuple(st.text_input('Enter the co-ordinate for the bottom-left point', '0, 0.75').split(','),)
    p4 = tuple(st.text_input('Enter the co-ordinate for the bottom-right point', '1,0.75').split(','))

    points = np.float32([p1, p2, p3, p4])

    cfg['warp'] = points

    image_points = processor.draw_points(points)
    st.subheader("ROI for Perspective Warp")
    st.image(image_points, use_column_width=True)

    #if st.button('Proceed'):
    st.subheader("Perspective transformed image")
    warped = processor.warp(points)        
    st.image(warped, use_column_width=True)

hls = st.checkbox("Visualize HLS Channels")
h, l, s = processor.get_hls()

if hls:
    st.subheader("Hue Channel")
    st.image(h, use_column_width=True)

    st.subheader("Lightness Channel")
    st.image(l, use_column_width=True)

    st.subheader("Saturation Channel")
    st.image(s, use_column_width=True)

st.write("Which channels would you like to select for thresholding?")

hls_select = st.multiselect("Select HLS Channels", ["Hue", "Lightness", "Saturation"], default=["Hue", "Saturation"])
cfg['threshold_channels'] = hls_select

if hls_select:
    thresholded_images = []
    st.subheader("Selected Channels")
    for channel in hls_select:
        if channel == "Hue":
            st.subheader("Hue Channel (Thresholded)")
            #st.image(h, use_column_width=True)
            range = st.slider("Select the range for thresholding", 0, 255, (20, 50))
            cfg['h_thresh'] = tuple(range)
            h_binary = processor.threshold(h, thresh = tuple(range))
            st.image(h_binary, use_column_width=True)
            thresholded_images.append(h_binary)
            #st.subheader("Thresholded Hue Channel")
        elif channel == "Lightness":
            st.subheader("Lightness Channel (Thresholded)")
            #st.image(l, use_column_width=True)
            range = st.slider("Select the range for thresholding", 0, 255, (150, 255))
            cfg['l_thresh'] = tuple(range)
            st.subheader("Thresholded Lightness Channel")
            l_binary = processor.threshold(l, thresh = tuple(range))
            st.image(l_binary, use_column_width=True)
            thresholded_images.append(l_binary)
        elif channel == "Saturation":
            st.subheader("Saturation Channel (Thresholded)")
            #st.image(s, use_column_width=True)
            range = st.slider("Select the range for thresholding", 0, 255, (100, 255))
            cfg['s_thresh'] = tuple(range)
            s_binary = processor.threshold(s, thresh = tuple(range))
            st.image(s_binary, use_column_width=True)
            thresholded_images.append(s_binary)
        
if len(hls_select) == 0:
    st.write("Please select at least one channel to proceed")
else:
    binary = processor.combine(thresholded_images)
    st.subheader("Final Thresholded Image")
    if len(hls_select) == 1:
        st.write("This image is the result of thresholding the channel: ", hls_select[0])
    else:
        ch_str = ""
        for channel in hls_select:
            ch_str += channel + ", "
        st.write("This image is the result of bitwise AND operation the channels: {}".format(ch_str))
    st.image(binary, use_column_width=True)


st.subheader("Morphological operations")
st.write("Would you like to apply any morphological operations?")


erode = st.checkbox("Erode", False)
cfg['erode'] = erode

if erode and len(hls_select) > 0:
    st.subheader("Eroded Image")
    kernel_erode = st.slider("Select the kernel size for erosion operation", min_value = 3, max_value = 21, step = 2, value = 3)
    cfg['kernel_erode'] = kernel_erode
    binary = processor.erode(binary, kernel_erode)
    st.image(binary, use_column_width=True)

dilate = st.checkbox("Dilate", False)
cfg['dilate'] = dilate

if dilate and len(hls_select) > 0:
    st.subheader("Dilated Image")
    kernel_dilate = st.slider("Select the kernel size for dilation operation", min_value = 3, max_value = 21, step = 2, value = 3)
    cfg['kernel_dilate'] = kernel_dilate
    binary = processor.dilate(binary, kernel_dilate)
    st.image(binary, use_column_width=True)

# open = st.checkbox("Open", False)
# cfg['open'] = open
# if open and len(hls_select) > 0:
#     st.subheader("Opened Image")
#     kernel_open = st.slider("Select the kernel size for opening operation", min_value = 3, max_value = 21, step = 2, value = 3)
#     cfg['kernel_open'] = kernel_open
#     binary = processor.open(binary, kernel_open)
#     st.image(binary, use_column_width=True)

opening = st.checkbox("Open", False)
cfg['opening'] = opening
if opening and len(hls_select) > 0:
    st.subheader("Opened Image")
    kernel_open = st.slider("Select the kernel size for opening operation", min_value = 3, max_value = 21, step = 2, value = 3)
    cfg['kernel_open'] = kernel_open
    binary = processor.open(binary, kernel_open)
    st.image(binary, use_column_width=True)

close = st.checkbox("Close", False)
cfg['close'] = close
if close and len(hls_select) > 0:
    st.subheader("Closed Image")
    kernel_close = st.slider("Select the kernel size for closing operation", min_value = 3, max_value = 21, step = 2, value = 3)
    cfg['kernel_close'] = kernel_close
    binary = processor.close(binary, kernel_close)
    st.image(binary, use_column_width=True)

# Insert a line / separator

st.markdown("""---""")

largcon = st.checkbox("Get largest contour", True)
cfg['largcon'] = largcon
if largcon and len(hls_select) > 0:
    st.subheader("Largest Contour")
    contours, binary = processor.get_largest_contour(binary)
    st.image(binary, use_column_width=True)
    viz = st.checkbox("Visualize Contours")
    if viz:
        st.subheader("All Contours")
        img_contours = processor.draw_contours(contours)
        st.image(img_contours, use_column_width=True)

blur = st.checkbox("Gaussian Blur", False)
cfg['blur'] = blur
if blur and len(hls_select) > 0:
    st.subheader("Blurred Image")
    kernel_size = st.slider("Select the kernel size", min_value = 3, max_value = 21, step = 2, value = 13)
    binary = processor.blur(binary, kernel_size)
    st.image(binary, use_column_width=True)
    cfg['blur_kernel'] = kernel_size

st.markdown("""---""")
st.subheader("Lane Detection")

if len(hls_select) > 0:
    st.write("Which technique would you like to use for lane detection?")
    lane_detection = st.radio("Select a technique", ("Manual", "BN Search"), index = 1)

    cfg['lane_detection'] = lane_detection
    if lane_detection == "Manual":
        thinning = st.checkbox("Thinning", False)
        if thinning:
            if not blur:
                st.write("Performing Gaussian Blur is recommended before thinning operation")
            st.subheader("Thinned Image")
            binary = processor.thinning(binary)
            st.image(binary, use_column_width=True)

        step_size = st.slider("Select the step size for search", min_value = 10, max_value = 100, step = 10, value = 50)
        cfg['thin_step'] = step_size

        st.subheader("Search Results")
        points = processor.get_points(binary, step = step_size)
        points_img = processor.plot_points(binary, points)
        st.image(points_img, use_column_width=True)

        df = pd.DataFrame(points, columns=["X", "Y"])
        st.write("Number of points found: ", len(points))
        st.write("Co-ordinates of the points:")
        st.write(df)
    
    elif lane_detection == "BN Search":
        st.subheader("BN Search")
        # select if circle or ellipse:
        st.write("Which shape would you like to use for the search?")
        shape = st.radio("Select a shape", ("Circle", "Ellipse"), index = 1)
        cfg['shape'] = shape

        draw = st.checkbox("Show search results", value = True)
        #cfg['draw'] = draw

        if shape == "Circle":
            st.subheader("Parameters for Circle Search")
            radius = st.slider("Select the radius of the search", min_value = 10, max_value = 100, step = 10, value = 50)
            cfg['radius'] = radius
            start_angle, end_angle = st.slider(
                "Select the arc angles to search within",
                min_value = 90,
                max_value = 450,
                value = (210, 330),
                step = 10
            )
            cfg['angles'] = (start_angle, end_angle)
            searcher = BNSearch(
                search_method = 'circle',
                radius = radius,
                start_angle = start_angle,
                end_angle = end_angle,
                draw = draw
            )
        if shape == "Ellipse":
            st.subheader("Parameters for Ellipse Search")
            a = st.slider("Select the major axis length", min_value = 20, max_value = 200, step = 10, value = 100)
            b = st.slider("Select the minor axis length", min_value = 10, max_value = 100, step = 10, value = 50)
            cfg['ellipse_ab'] = (a, b)
            start_angle, end_angle = st.slider(
                "Select the arc angles to search within",
                min_value = 90,
                max_value = 450,
                value = (210, 330),
                step = 10
            )
            cfg['angles'] = (start_angle, end_angle)
            searcher = BNSearch(
                search_method = 'ellipse',
                a = a,
                b = b,
                start_angle = start_angle,
                end_angle = end_angle,
                draw = draw
            )
        
        st.subheader("Search Results")
        points = searcher(binary)
        df = pd.DataFrame(points, columns=["X", "Y"])

        st.image(searcher.img_color, use_column_width=True)

        st.write("Number of points found: ", len(points))
        st.write("Co-ordinates of the points:")
        st.write(df)

# Get the name of the config file
cfg_file = st.text_input("Enter the name of the config file", "cfg.toml")

st.write("Click the button below to save the configuration file")
if st.button("Save Config"):
    # Save the cfg file as TOML
    with open('data/{}'.format(cfg_file), 'w') as f:
        toml.dump(cfg, f)
    st.write("Config file saved successfully to data/{}".format(cfg_file))