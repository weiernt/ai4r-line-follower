# Library Imports
import cv2
from detector_lib import *

config = {
    "hls_select": ["Hue", "Saturation"],
    "h_range": (0, 100),
    "s_range": (50, 255),
    "morph": True,
    "erosion": True,
    "dilation": False,
    "kernel_erode": (3, 3),
    "kernel_dilate": (5, 5),
    "kernel_open": None,
    "kernel_close": None,
    "largcon": True,
    "blur": False,
    "kernel_blur": None,
    "other_view": True,
    "lane_detection": "Manual",
    "thinning": True,
    "step_size": 10,
    "draw": True
}

import cv2

# Image Processing Loop
def process_image(image, config):
    hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
    h, l, s = cv2.split(hls)

    # Extracting parameters from the config dictionary
    hls_select = config.get("hls_select", [])
    h_range = config.get("h_range", None)
    l_range = config.get("l_range", None)
    s_range = config.get("s_range", None)
    morph = config.get("morph", False)
    erosion = config.get("erosion", False)
    dilation = config.get("dilation", False)
    opening = config.get("opening", False)
    closing = config.get("closing", False)
    kernel_erode = config.get("kernel_erode", None)
    kernel_dilate = config.get("kernel_dilate", None)
    kernel_open = config.get("kernel_open", None)
    kernel_close = config.get("kernel_close", None)
    largcon = config.get("largcon", False)
    blur = config.get("blur", False)
    kernel_blur = config.get("kernel_blur", None)
    other_view = config.get("other_view", False)
    lane_detection = config.get("lane_detection", None)
    thinning = config.get("thinning", False)
    step_size = config.get("step_size", None)
    draw = config.get("draw", False)
    shape = config.get("shape", None)
    radius = config.get("radius", None)
    start_angle = config.get("start_angle", None)
    end_angle = config.get("end_angle", None)
    a = config.get("a", None)
    b = config.get("b", None)

    # Threshold Image

    thresholded = []

    if not len(hls_select) == 0:
        for channel in hls_select:
            if channel == "Hue":
                h_binary = thresh_image(h, h_range)
                thresholded.append(h_binary)
            elif channel == "Lightness":
                l_binary = thresh_image(l, l_range)
                thresholded.append(l_binary)
            elif channel == "Saturation":
                s_binary = thresh_image(s, s_range)
                thresholded.append(s_binary)

        im_thresh = combine_images(thresholded)

        # Show the thresholded image
        if threshold_view:
            threshold_placeholder.image(
                im_thresh, caption="Thresholded Image", use_column_width=True)

    # Morphological Operations

    if morph:
        if erosion:
            im_thresh = erode(im_thresh, kernel_erode)
        if dilation:
            im_thresh = dilate(im_thresh, kernel_dilate)
        if opening:
            im_thresh = open_op(im_thresh, kernel_open)
        if closing:
            im_thresh = close_op(im_thresh, kernel_close)

    # Other postprocessing operations

    if largcon:
        im_thresh = get_largest_contour(im_thresh)

    if blur:
        im_thresh = gauss_blur(im_thresh, kernel_blur)

    if other_view:
        other_placeholder.image(
            im_thresh, caption="Processed Image", use_column_width=True)

    if lane_detection == "Manual":
        if thinning:
            im_thresh = thin_op(im_thresh)
        points = manual_search(im_thresh, step_size)
        if draw:
            points_img = plot_points(im_thresh, points)
            return points_img

    elif lane_detection == "BN Search":
        if shape == "Circle":
            ld = LineDetectorCircle(radius, start_angle, end_angle, draw=draw)
        elif shape == "Ellipse":
            ld = LineDetectorEllipse(a, b, start_angle, end_angle, draw=draw)
        points = ld(im_thresh)
        if draw:
            image = ld.img_color
            return image
    return None


# Call the process_image function with the image and config dictionary
result = process_image(image, config)


class ImageProcessor:
    def __init__(self, image, config):
        self.image = image
        self.config = config
        self.hls = cv2.cvtColor(self.image, cv2.COLOR_BGR2HLS)

    def extract_params(self, config):
        self.hls_select = config.get("hls_select", [])
        self.h_range = config.get("h_range", None)
        self.l_range = config.get("l_range", None)
        self.s_range = config.get("s_range", None)
        self.morph = config.get("morph", False)
        self.erosion = config.get("erosion", False)
        self.dilation = config.get("dilation", False)
        self.opening = config.get("opening", False)
        self.closing = config.get("closing", False)
        self.kernel_erode = config.get("kernel_erode", None)
        self.kernel_dilate = config.get("kernel_dilate", None)
        self.kernel_open = config.get("kernel_open", None)
        self.kernel_close = config.get("kernel_close", None)
        self.largcon = config.get("largcon", False)
        self.blur = config.get("blur", False)
        self.kernel_blur = config.get("kernel_blur", None)
        self.other_view = config.get("other_view", False)
        self.lane_detection = config.get("lane_detection", None)
        self.thinning = config.get("thinning", False)
        self.step_size = config.get("step_size", None)
        self.draw = config.get("draw", False)
        self.shape = config.get("shape", None)
        self.radius = config.get("radius", None)
        self.start_angle = config.get("start_angle", None)
        self.end_angle = config.get("end_angle", None)
        self.a = config.get("a", None)
        self.b = config.get("b", None)
    
    def process_image(self):
        h, l, s = cv2.split(self.hls)

        ... continue

