import cv2
import datetime

# Create a VideoCapture object and specify the device index for your webcam
cap = cv2.VideoCapture(0)

# Set frame rate
cap.set(cv2.CAP_PROP_FPS, 10)  # Replace 30 with your desired frame rate

# Check if the parameters were successfully set
print("Frame rate:", cap.get(cv2.CAP_PROP_FPS))

# Define variables for video recording
recording = False
video_writer = None
filename = ""

# Define a function to start or stop video recording
def toggle_recording():
    global recording, video_writer, filename
    if recording:
        recording = False
        video_writer.release()
        print('Video recording stopped and saved to disk as: {}'.format(filename))
    else:
        recording = True
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        filename = 'streamcam/record_{}.avi'.format(timestamp)
        video_writer = cv2.VideoWriter(filename, cv2.VideoWriter_fourcc(*'XVID'), cap.get(cv2.CAP_PROP_FPS), (int(cap.get(3)), int(cap.get(4))))
        print('Video recording started. Press Enter again to stop recording.')

print('Press Enter to start recording. Press Enter again to stop recording.')

# Use a while loop to capture frames from the webcam and listen for a console input event to trigger video recording
while True:
    ret, frame = cap.read()

    command = input()

    if command == '':
        toggle_recording()
        if recording:
            video_writer.write(frame)
    elif command == 'q':
        break

# Release the video capture object and video writer
cap.release()
if recording:
    video_writer.release()
