import cv2
import streamlit as st
import time

# Function to set video capture properties
def set_capture_properties(capture, resolution, frame_rate):
    width, height = get_resolution(resolution)
    capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    capture.set(cv2.CAP_PROP_FPS, frame_rate)

# Function to get resolution tuple from selected resolution
def get_resolution(resolution):
    resolutions = {
        "480p": (640, 480),
        "540p": (960, 540),
        "720p": (1280, 720),
        "1080p": (1920, 1080)
    }
    return resolutions[resolution]

res_list = ["480p", "540p", "720p", "1080p"]

frame_rates = {
    "480p": (5, 10, 15, 20, 30, 60),
    "540p": (5, 10, 15, 20, 30, 60),
    "720p": (5, 10, 15, 20, 30, 60),
    "1080p": (5, 10, 15, 20, 30, 60)
}

# Streamlit app
def main():
    version = 0
    #st.title("OpenCV Video Capture")
    # Resolution selection
    res = st.radio("Select resolution:", res_list, index=0, key="resolution", horizontal = True)

    # Frame rate selection
    frame_rate = st.radio("Select frame rate:", frame_rates[res], index=0, key="frame_rate", horizontal = True)

    # Create video capture object
    capture = cv2.VideoCapture(0)

    # Set capture properties
    set_capture_properties(capture, res, frame_rate)
    
    # Create placeholders for displaying real-time data
    frame_rate_placeholder = st.empty()
    resolution_placeholder = st.empty()
    frame_placeholder = st.empty()

    # Start capturing and displaying frames
    stop_button = st.button("Stop", key = 'stop')

    while not stop_button:
        ret, frame = capture.read()

        # Check if frame is not None before displaying
        if frame is not None:
            # Display frame
            frame_placeholder.image(frame, channels="BGR")

            # Display current frame rate
            current_frame_rate = capture.get(cv2.CAP_PROP_FPS)
            frame_rate_placeholder.text(f"Current frame rate: {current_frame_rate:.2f}")

            # Display current resolution
            current_width = capture.get(cv2.CAP_PROP_FRAME_WIDTH)
            current_height = capture.get(cv2.CAP_PROP_FRAME_HEIGHT)
            resolution_placeholder.text(f"Resolution: {current_width}x{current_height}")
        else:
            frame_placeholder.text("Waiting for the camera to initialize...")
            time.sleep(1)
            capture.release()
            capture = cv2.VideoCapture(0)
            set_capture_properties(capture, res, frame_rate)
    
    # Release the capture object
    capture.release()

if __name__ == "__main__":
    main()
