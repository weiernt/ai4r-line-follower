import cv2
import datetime

# Create a VideoCapture object and specify the device index for your webcam
cap = cv2.VideoCapture(0)

# Define a function to capture and store an image with a timestamp
def capture_image(impath):
    ret, frame = cap.read()
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    filename = f'{impath}_{timestamp}.jpg'
    cv2.imwrite(filename, frame)
    print('Image captured and saved to disk as: {}'.format(filename))

impath = 'streamcam/test_'

# Use a while loop to capture frames from the webcam and listen for a key event to trigger the image capture function
while True:
    ret, frame = cap.read()
    cv2.imshow('frame', frame)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break
    elif key == ord('c'):
        capture_image(impath)

# Release the video capture object and destroy any open windows
cap.release()
cv2.destroyAllWindows()