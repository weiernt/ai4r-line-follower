import cv2
import streamlit as st
import time
import os

# Setting up placeholders in the right column

res_list = ["480p", "540p", "720p", "1080p"]

frame_rates = {
    "480p": (5, 10, 15, 20, 30),
    "540p": (5, 10, 15),
    "720p": (5, 10),
    "1080p": (5,)
}

frame_buffer = []
frame_limit = 100
output_dir = 'capture'

# Function to get resolution tuple from selected resolution
def get_resolution(resolution):
    resolutions = {
        "480p": (640, 480),
        "540p": (960, 540),
        "720p": (1280, 720),
        "1080p": (1920, 1080)
    }
    return resolutions[resolution]

# Function to set video capture properties
def set_capture_properties(capture, resolution, frame_rate):
    width, height = get_resolution(resolution)
    capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    capture.set(cv2.CAP_PROP_FPS, frame_rate)

def main():
    st.title("Frame Capture Tool")

    name_run = st.text_input("Enter name of run: ", value = "run_1")
    out_run = os.path.join(output_dir, name_run)
    os.makedirs(out_run, exist_ok=True) # Create the output directory if it doesn't exist

    # input text
    frame_limit = st.number_input(
        "Enter number of frames to capture: ",
        min_value = 1, max_value = 1000, value = 100, step = 1)

    res = st.radio("Select resolution: ", res_list, index = 0, horizontal = True)
    fr_opts = frame_rates[res]
    frame_rate = st.radio("Select frame rate: ", fr_opts, index = len(fr_opts) - 1, horizontal = True)

    stream_fps_placeholder = st.empty()
    stream_res_placeholder = st.empty()
    frame_placeholder = st.empty()
    info_placeholder = st.empty()

    cap = cv2.VideoCapture(0)
    set_capture_properties(cap, res, frame_rate)

    start_button = st.checkbox('Start Capture', key = 'start')
    stop_button = st.button("Stop", key = 'stop')

    counter = 0

    while not stop_button:
        ret, frame = cap.read()

        if frame is not None:
            current_frame_rate = cap.get(cv2.CAP_PROP_FPS)
            stream_fps_placeholder.text(f"Current frame rate: {current_frame_rate:.2f}")
            current_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
            current_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
            stream_res_placeholder.text(f"Current resolution: {current_width} x {current_height}")
            frame_placeholder.image(frame, channels = "BGR")

            if start_button and counter < frame_limit:
                frame_buffer.append(frame)
                counter += 1
                info_placeholder.text(f"Capturing frame {counter} of {frame_limit} ...")
            else: 
                if len(frame_buffer) == frame_limit:
                    info_placeholder.text("Saving frames ...")
                    for i, frame in enumerate(frame_buffer):
                        cv2.imwrite(f"{out_run}/frame_{i}.jpg", frame)
                    info_placeholder.text("Done!")
                    frame_buffer.clear()
                    #counter = 0
        else:
            frame_placeholder.text("Wating for the camera to initialize ...")
            time.sleep(1)
            cap.release()
            cap = cv2.VideoCapture(0)
            set_capture_properties(cap, res, frame_rate)

    cap.release()

if __name__ == '__main__':
    main()