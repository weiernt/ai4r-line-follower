import cv2
import numpy as np

image_path = 'samples/gray_carpet_right.jpg'

image = cv2.imread(image_path)
hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)

h, l, s = cv2.split(hls)

#h_binary = cv2.threshold(h, 20, 50, cv2.THRESH_BINARY)[1]

#h_binary = np.where()

h_binary = cv2.inRange(h, 20, 50)

#l_binary = cv2.threshold(l, 100, 255, cv2.THRESH_BINARY)[1]
#s_binary = cv2.threshold(s, 100, 255, cv2.THRESH_BINARY)[1]

cv2.imshow('h', h_binary)
#cv2.imshow('s', s_binary)
#cv2.imshow('l', l_binary)

cv2.waitKey(0)
cv2.destroyAllWindows()
