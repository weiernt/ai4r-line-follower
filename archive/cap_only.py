import cv2
import datetime

# Create a VideoCapture object and specify the device index for your webcam
cap = cv2.VideoCapture(0)

# Define a function to capture and store an image with a timestamp
def capture_image(impath):
    ret, frame = cap.read()
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    filename = f'{impath}_{timestamp}.jpg'
    cv2.imwrite(filename, frame)
    print('Image captured and saved to disk as: {}'.format(filename))

impath = 'streamcam/test_'

print('Enter "q" to quit and Return Key to capture an image.')

# Use a while loop to capture frames from the webcam and listen for a console input event to trigger the image capture function
while True:
    ret, frame = cap.read()

    command = input()

    if command == 'q':
        break
    elif command == '':
        capture_image(impath)

# Release the video capture object
cap.release()