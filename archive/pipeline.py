# Library Imports
import toml
from detector_lib import *

# Load Config File
config_path = 'configs/config_1.toml'
config = toml.load(config_path)

# What are the different params that we will be looking for in the config file? 
"""
- hls_select
- morph
    - erosion
    - dilation
    - opening
    - closing
- largcon
- 


"""

# Use the Detector Lib for Line Detection

# Image Processing Loop
def process_image(image, **configs):
    hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
    h, l, s = cv2.split(hls)

    # Threshold Image

    thresholded = []

    if not len(hls_select) == 0:
        for channel in hls_select:
            if channel == "Hue":
                h_binary = thresh_image(h, h_range)
                thresholded.append(h_binary)
            elif channel == "Lightness":
                l_binary = thresh_image(l, l_range)
                thresholded.append(l_binary)
            elif channel == "Saturation":
                s_binary = thresh_image(s, s_range)
                thresholded.append(s_binary)

        im_thresh = combine_images(thresholded)

        # Show the thresholded image
        if threshold_view:
            threshold_placeholder.image(
                im_thresh, caption = "Thresholded Image", use_column_width = True)

    # Morphological Operations

    if morph:
        if erosion:
            im_thresh = erode(im_thresh, kernel_erode)
        if dilation:
            im_thresh = dilate(im_thresh, kernel_dilate)
        if opening:
            im_thresh = open_op(im_thresh, kernel_open)
        if closing:
            im_thresh = close_op(im_thresh, kernel_close)

    # Other postprocessing operations

    if largcon:
        im_thresh = get_largest_contour(im_thresh)

    if blur:
        im_thresh = gauss_blur(im_thresh, kernel_blur)

    if other_view:
        other_placeholder.image(
            im_thresh, caption = "Processed Image", use_column_width = True)

    if lane_detection == "Manual":
        if thinning:
            im_thresh = thin_op(im_thresh)    
        points = manual_search(im_thresh, step_size)
        if draw:
            points_img = plot_points(im_thresh, points)
            return points_img

    elif lane_detection == "BN Search":
        if shape == "Circle":
            ld = LineDetectorCircle(radius, start_angle, end_angle, draw = draw)
        elif shape == "Ellipse":
            ld = LineDetectorEllipse(a, b, start_angle, end_angle, draw = draw)
        points = ld(im_thresh)
        if draw:
            image = ld.img_color
            return image
    return None