"""

What are the different preprocessing steps that we can use? 

- Gaussian Blur
- Contrast Enhancement
- Color Balance Adjustment
- Thresholding
- Morphological Operations


"""

import cv2
import pickle
import numpy as np

def load_rgb_image(img_path):
    """
    Load an RGB image from the specified path
    """
    return cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)

def undistort_image(img, cal_path='camera_cal/cal_pickle.p'):
    #cv2.imwrite('camera_cal/test_cal.jpg', dst)
    with open(cal_path, mode='rb') as f:
        file = pickle.load(f)
    mtx = file['mtx']
    dist = file['dist']
    dst = cv2.undistort(img, mtx, dist, None, mtx)
    return dst

def perspective_warp(img, 
                     dst_size=(1280,720),
                     src=np.float32([(0.3,0.45),(0.8,0.47),(0,0.8),(1,0.75)]),
                     dst=np.float32([(0,0), (1, 0), (0,1), (1,1)])):
    img_size = np.float32([(img.shape[1],img.shape[0])])
    src = src* img_size
    # For destination points, I'm arbitrarily choosing some points to be
    # a nice fit for displaying our warped result 
    # again, not exact, but close enough for our purposes
    dst = dst * np.float32(dst_size)
    # Given src and dst points, calculate the perspective transform matrix
    M = cv2.getPerspectiveTransform(src, dst)
    # Warp the image using OpenCV warpPerspective()
    warped = cv2.warpPerspective(img, M, dst_size)
    return warped


def gauss_blur(img, filter_size = 5):
    """
    Apply Gaussian Blur to the image
    """
    return cv2.GaussianBlur(img, (filter_size, filter_size), 0)

def median_blur(img, filter_size = 5):
    """
    Apply Median Blur to the image
    """
    return cv2.medianBlur(img, filter_size)

def hist_eq(img):
    """
    Apply Histogram Equalization to the image
    """
    return cv2.equalizeHist(img)

def clahe(img, clip_limit = 2.0, tile_size = 8):
    """
    Apply Contrast Limited Adaptive Histogram Equalization to the image
    """
    clahe = cv2.createCLAHE(clipLimit = clip_limit, tileGridSize = (tile_size, tile_size))
    return clahe.apply(img)

def color_balance(img):
    """
    Apply Color Balance Adjustment to the image
    (Expected input is in RGB format)
    """
    result = cv2.cvtColor(img, cv2.COLOR_RGB2LAB)
    avg_a = np.average(result[:, :, 1])
    avg_b = np.average(result[:, :, 2])
    result[:, :, 1] = result[:, :, 1] - ((avg_a - 128) * (result[:, :, 0] / 255.0) * 1.1)
    result[:, :, 2] = result[:, :, 2] - ((avg_b - 128) * (result[:, :, 0] / 255.0) * 1.1)
    return cv2.cvtColor(result, cv2.COLOR_LAB2RGB)

def opening(img, kernel_size = 5):
    """
    Apply Opening to the image
    Input is expected to be a binary image
    """
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    return cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)

def closing(img, kernel_size = 5):
    """
    Apply Closing to the image
    Input is expected to be a binary image
    """
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    return cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)

def get_hls(img):
    """
    Convert the image from RGB to HLS color space
    And return the H, L and S channels
    Input is expected to be in RGB format
    """
    hls = cv2.cvtColor(img, cv2.COLOR_RGB2HLS)
    h_channel = hls[:, :, 0]
    l_channel = hls[:, :, 1]
    s_channel = hls[:, :, 2]
    return h_channel, l_channel, s_channel

# Binary Thresholding:
# This technique is used to convert a grayscale image to a binary image.
# The technique involves comparing each pixel value with a threshold value.

def binary_thresh(img, thresh = (0, 255)):
    """
    Apply a binary threshold to the image
    """
    binary_output = np.zeros_like(img)
    binary_output[(img >= thresh[0]) & (img <= thresh[1])] = 1
    return binary_output

# Adaptive Mean Thresholding:
# This technique calculates the mean of the neighborhood of each pixel and 
# subtracts a constant from it to obtain the threshold value. The size of the 
# neighborhood and the constant value can be specified by the user.

def adaptive_mean_thresh(img, block_size = 11, c = 2):
    """
    Apply Adaptive Mean Thresholding to the image
    """
    return cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, block_size, c)

# Adaptive Gaussian Thresholding:
# This technique calculates the weighted mean of the neighborhood of each pixel
# using a Gaussian kernel and subtracts a constant from it to obtain the 
# threshold value. The size of the neighborhood, the standard deviation of the
# Gaussian kernel, and the constant value can be specified by the user.

def adaptive_gauss_thresh(img, block_size = 11, c = 2):
    """
    Apply Adaptive Gaussian Thresholding to the image
    """
    return cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, block_size, c)

# Otsu's Threshold: This technique automatically calculates the threshold value
# based on the histogram of the image. It assumes that the image contains two 
# classes of pixels, foreground, and background.

def otsu_thresh(img):
    """
    Apply Otsu's Thresholding to the image
    """
    return cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

def thinning(img):
    """
    Apply Thinning to the image
    """
    return cv2.ximgproc.thinning(img)

def largest_contour(binary_img):
    """
    Return the largest contour in the image
    Input / Output: Binary Image
    """
    contours, hierarchy = cv2.findContours(binary_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea, reverse=True)
    largest_contour = contours[0]
    binary_img = np.zeros_like(binary_img)
    cv2.drawContours(binary_img, [largest_contour], -1, 255, -1)
    return binary_img

def erosion(img):
    """
    Apply Erosion to the image
    """
    kernel = np.ones((5, 5), np.uint8)
    return cv2.erode(img, kernel, iterations = 1)

def detect_line(img):
    """
    Detect the line in the image
    """
    edges = cv2.Canny(img, 100, 200)
    lines = cv2.HoughLinesP(edges, 1, np.pi/180, 100, minLineLength = 100, maxLineGap = 10)
    return lines


