import os
import cv2
import pandas as pd
import streamlit as st
from detector_lib import load_config, list_jpg_files_in_directory
from bench_lib import *

st.set_page_config(layout='wide')
st.title("Line Detection Benchmarking tool")

left_column, right_column = st.columns(2)

def main():
    # with st.sidebar:
    #     # Add a checkbox to show the contents of the YAML files
    #     show_yaml_contents = st.checkbox("Show YAML Contents")

    with left_column:

        st.subheader("Source")
        src = st.radio("Select source", ("Path", "Directory"))

        if src == "Path":
            dir_path = st.text_input("Enter path to directory", "samples/streamcam/new_tape")
            if dir_path:
                if os.path.exists(dir_path):
                    # Get the list of files in the selected directory
                    files = list_jpg_files_in_directory(dir_path)
                    # Display a drop-down box with all files in the directory
                    image_path = st.selectbox("Select a file:", files)
                    image_path = os.path.join(dir_path, image_path)
        elif src == "Directory":
            dir_root = st.text_input("Enter path to directory root", "samples/streamcam/")
            if dir_root:
                if os.path.exists(dir_root):
                    # Get the list of dirs in the selected directory
                    dirs = os.listdir(dir_root)
                    # Display a drop-down box with all dirs in the directory
                    dir_path = st.selectbox("Select a directory:", dirs)
                    dir_path = os.path.join(dir_root, dir_path)

        # Select config file for testing
        st.subheader("Config for testing")
        st.write("Place your config file in the configs/ directory for it to appear here")
        CONFIGS = os.listdir("configs/")
        config_test = st.selectbox("Select config file for testing:", CONFIGS)
        
        # Select config file for benchmarking
        st.subheader("Config for benchmarking")
        st.write("Place your config file in the configs/ directory for it to appear here")
        
        CONFIGS = os.listdir("configs/")
        # Check if the default file exists in the list of configurations
        default_config = "config_benchmark.yaml"
        if default_config not in CONFIGS:
            default_config = CONFIGS[0]  # Set the first config as default if the benchmark config doesn't exist

        config_bench = st.selectbox("Select config file for benchmarking:", CONFIGS, index=CONFIGS.index(default_config))
        
        show_yaml_contents = st.checkbox("Show YAML Contents", False)

        # Read the YAML files
        config_test_path = f"configs/{config_test}"
        config_test_content = read_yaml_file(config_test_path)
        
        config_bench_path = f"configs/{config_bench}"
        config_bench_content = read_yaml_file(config_bench_path)

        # Display YAML contents if the checkbox is checked
        if show_yaml_contents:
            st.subheader("YAML Contents - Config for testing")
            with st.markdown("```yaml"):
                st.code(config_test_content)
            st.subheader("YAML Contents - Config for benchmarking")
            with st.markdown("```yaml"):
                st.code(config_bench_content)
        
        # Load the config files
        config_test = load_config(f"configs/{config_test}")
        config_bench = load_config(f"configs/{config_bench}")

    with right_column:
        st.subheader("Benchmark Results:")
        draw = st.checkbox("Draw points on image", value = True)
        if draw:
            image_placeholder = st.empty()
        benchmark_placeholder = st.empty()

    bench_obj = BenchMSE(config_test, config_bench, draw = draw)
    
    if src == "Path":
        try:
            img = cv2.imread(image_path)
            img, mse_nearest_point, mse_slope = bench_obj.benchmark(img)
            if draw:
                image_placeholder.image(img, channels="BGR")
            benchmark_placeholder.markdown(
                f"""
                ```
                MSE Nearest Point: {mse_nearest_point:.4f}
                MSE from Slope:    {mse_slope:.4f}
                ```
                """
            )
        except:
            benchmark_placeholder.warning("Please select a valid image file")
    elif src == "Directory":
        try:
            file_list = os.listdir(dir_path)
            benches = []
            for file in file_list:
                img_path = os.path.join(dir_path, file)
                img = cv2.imread(img_path)
                img, mse_nearest_point, mse_slope = bench_obj.benchmark(img)
                if draw:
                    image_placeholder.image(img, channels="BGR")

                benchmark_placeholder.markdown(
                    f"""
                    ```
                    MSE Nearest Point: {mse_nearest_point:.4f}
                    MSE Slope: {mse_slope:.4f}
                    ```
                    """
                )
                benches.append({
                    'file_name': file,
                    'mse_nearest_point': mse_nearest_point,
                    'mse_slope': mse_slope
                })

            # Create lists to store the mse_nearest_point and mse_slope values from the benches list
            mse_nearests = [bench['mse_nearest_point'] for bench in benches]
            mse_slopes = [bench['mse_slope'] for bench in benches]

            mse_nearest_mean = np.mean(filter_outliers(mse_nearests, threshold = 20))
            mse_slope_mean = np.mean(filter_outliers(mse_slopes, threshold = 10))

            benches_df = pd.DataFrame(benches)

            # Save the DataFrame to a CSV file
            csv_file_path = os.path.join("bench", "bench_data.csv")
            # make sure the directory exists 
            os.makedirs(os.path.dirname(csv_file_path), exist_ok=True)
            benches_df.to_csv(csv_file_path, index=False)

            benchmark_placeholder.markdown(
                f"""
                ```
                SUMMARY: 
                ------------------------------------------
                MSE Nearest Point Mean: {mse_nearest_mean:.4f}
                MSE Slope Mean: {mse_slope_mean:.4f}
                ```

                [Download Summary and Bench Data CSV]({csv_file_path})
                """
            )
        except Exception as e:
            print(e)
            benchmark_placeholder.warning("Please select a valid directory")


if __name__ == '__main__':
    main()