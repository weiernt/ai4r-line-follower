import cv2
import yaml
import numpy as np
import os

def list_jpg_files_in_directory(directory):
    jpg_files = [file for file in os.listdir(directory) if file.lower().endswith(".jpg")]
    return jpg_files


def get_resolution(resolution):
    resolutions = {
        "480p": (640, 480),
        "540p": (960, 540),
        "720p": (1280, 720),
        "1080p": (1920, 1080)
    }
    return resolutions[resolution]

def set_capture_properties(capture, resolution, frame_rate, exposure = None):
    """Set video capture properties"""
    width, height = get_resolution(resolution)
    capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    capture.set(cv2.CAP_PROP_FPS, frame_rate)
    if exposure is not None:
        capture.set(cv2.CAP_PROP_EXPOSURE, exposure)

class LineDetectorCircle:
    def __init__(
            self, radius = 50, start_angle=210, end_angle=330, draw = False):
        self.start_angle = start_angle
        self.end_angle = end_angle
        self.draw = draw

        self.hash_table = self.create_circle_hash_table(radius)

    @staticmethod
    def create_circle_hash_table(radius):
        hash_table = {
            'x': np.zeros(361, dtype=np.int8),
            'y': np.zeros(361, dtype=np.int8)
        }
        for angle in range(361):
            hash_table['x'][angle] = int(radius * np.cos(np.deg2rad(angle)))
            hash_table['y'][angle] = int(radius * np.sin(np.deg2rad(angle)))
        return hash_table

    def __call__(self, img):
        self.img = img
        self.img_shape_0 = img.shape[0]
        self.img_shape_1 = img.shape[1]
        if self.draw:
            self.img_color = cv2.cvtColor(self.img, cv2.COLOR_GRAY2BGR)
        
        start = self.get_starting_point()
        if start is None: return None

        #print('Starting point: ', start)

        points = [start, ]
        next_point = self.get_next_point(start)
        if next_point is not None:
            points.append(next_point)
            #print('Next point: ', next_point)

        if self.draw:
            cv2.circle(self.img_color, start, 3, (0, 0, 255), -1)
            if next_point is not None:
                cv2.circle(self.img_color, next_point, 3, (0, 0, 255), 1)

        while next_point is not None:
            theta = np.arctan2(points[-1][1] - points[-2][1], points[-1][0] - points[-2][0])
            delta = int(90 + np.rad2deg(theta))
            next_point = self.get_next_point(next_point, delta)
            if next_point is not None:
                points.append(next_point)
                #print('Next point: ', next_point)
        
        return points

    def get_next_point(self, current_point, delta = 0):
        points = self.get_points_on_circle(current_point, delta)

        # Check if any point is out of bounds
        valid_points_mask = np.logical_and.reduce((
            points[:, 0] >= 0,
            points[:, 0] < self.img_shape_1,
            points[:, 1] >= 0,
            points[:, 1] < self.img_shape_0
        ))

        # Return None if any of the points is invalid
        # if not np.all(valid_points_mask): return None       

        # TODO: Instead of stopping, we can just filter out valid points, 
        # and only stop if there are no more valid points to search for

        # Return None if more than half of the points are invalid
        if np.sum(valid_points_mask) < len(valid_points_mask) / 2:
            return None

        points = points[valid_points_mask]

        if self.draw:
            for point in points:
                cv2.circle(self.img_color, tuple(point), 3, (0, 255, 0), -1)
        
        # THERE WAS A BUG HERE WHICH HAS BEEN FIXED
        valid_mask = self.img[points[:, 1], points[:, 0]] > 0
        points_on_lane = points[valid_mask]

        # Return none if no point is found
        if len(points_on_lane) == 0: return None       

        next_point = points_on_lane[int(len(points_on_lane)/2)]

        if self.draw:
            for point in points_on_lane:
                cv2.circle(self.img_color, tuple(point), 3, (255, 0, 0), -1)
                # draw the next point
                cv2.circle(self.img_color, tuple(next_point), 3, (0, 0, 255), -1)

        # Return the median point (next point)
        return tuple(next_point)

    def get_points_on_circle(self, center, delta = 0):
        """
        Get points on a circle using the hash table
        """
        angles = np.arange(self.start_angle + delta, self.end_angle + delta) % 360
        x_offsets = self.hash_table['x'][angles]
        y_offsets = self.hash_table['y'][angles]
        points = np.column_stack((x_offsets + center[0], y_offsets + center[1]))
        return points
    
    def get_starting_point(self):
        last_idx = -1
        while abs(last_idx) < self.img_shape_0//2:
            last_row = self.img[last_idx, :]
            lane_exists = np.where(last_row > 0)[0] # lane exists where there is a nonzero value
            if len(lane_exists) > 0: # If lane exists
                lane_center = np.median(lane_exists)
                center_point = (int(lane_center), (self.img_shape_0 + last_idx))
                return center_point
            # Else move up by 1 pixel
            last_idx -= 1       # Removed BUG here
        return None

# Line Detector Class (Ellipse)

class LineDetectorEllipse:
    def __init__(
            self,a = 100, b = 50, start_angle=210, end_angle=330, draw = False):
        self.start_angle = start_angle
        self.end_angle = end_angle
        self.draw = draw
        self.a = a
        self.b = b
        self.thetas = np.deg2rad(np.arange(self.start_angle, self.end_angle, 1))
        self.cos_theta = np.cos(self.thetas)
        self.sin_theta = np.sin(self.thetas)

    def __call__(self, img):
        self.img = img
        self.img_shape_0 = img.shape[0]
        self.img_shape_1 = img.shape[1]
        if self.draw:
            self.img_color = cv2.cvtColor(self.img, cv2.COLOR_GRAY2BGR)
        start = self.get_starting_point()
        if start is None: return None

        #print('Starting point: ', start)

        points = [start, ]
        next_point = self.get_next_point(start)
        if next_point is not None:
            points.append(next_point)
            #print('Next point: ', next_point)

        if self.draw:
            cv2.circle(self.img_color, start, 3, (0, 0, 255), -1)
            if next_point is not None:
                cv2.circle(self.img_color, next_point, 3, (0, 0, 255), 1)

        while next_point is not None:
            theta = np.arctan2(points[-1][1] - points[-2][1], points[-1][0] - points[-2][0])
            delta = int(90 + np.rad2deg(theta))
            next_point = self.get_next_point(next_point, delta)
            if next_point is not None:
                points.append(next_point)
                #print('Next point: ', next_point)
        
        return points

    def get_next_point(self, current_point, delta = 0):
        points = self.get_points_on_ellipse(current_point, angle = delta)
        #points = np.array(points)

        # Check if any point is out of bounds
        valid_points_mask = np.logical_and.reduce((
            points[:, 0] >= 0,
            points[:, 0] < self.img_shape_1,
            points[:, 1] >= 0,
            points[:, 1] < self.img_shape_0
        ))

        # Return None if any of the points is invalid
        # if not np.all(valid_points_mask): return None       

        # TODO: Instead of stopping, we can just filter out valid points, 
        # and only stop if there are no more valid points to search for

        # Return None if more than half of the points are invalid
        if np.sum(valid_points_mask) < len(valid_points_mask) / 2:
            return None

        points = points[valid_points_mask]

        if self.draw:
            for point in points:
                cv2.circle(self.img_color, tuple(point), 3, (0, 255, 0), -1)
        
        # THERE WAS A BUG HERE WHICH HAS BEEN FIXED
        valid_mask = self.img[points[:, 1], points[:, 0]] > 0
        points_on_lane = points[valid_mask]

        # Return none if no point is found
        if len(points_on_lane) == 0: return None       

        next_point = points_on_lane[int(len(points_on_lane)/2)]

        if self.draw:
            for point in points_on_lane:
                cv2.circle(self.img_color, tuple(point), 3, (255, 0, 0), -1)
                # draw the next point
                cv2.circle(self.img_color, tuple(next_point), 3, (0, 0, 255), -1)

        # Return the median point (next point)
        return tuple(next_point)
    
    def get_points_on_ellipse(self, center, angle = 0):
        """
        get points on an ellipse with rotated axes
        """
        alpha = np.deg2rad(angle)
        cos_alpha = np.cos(alpha)
        sin_alpha = np.sin(alpha)

        x = np.int_(center[0] + self.a * self.cos_theta * cos_alpha - self.b * self.sin_theta * sin_alpha)
        y = np.int_(center[1] + self.a * self.cos_theta * sin_alpha + self.b * self.sin_theta * cos_alpha)

        points = np.vstack((x, y)).T
        return points

    def get_starting_point(self):
        last_idx = -1
        while abs(last_idx) < self.img_shape_0//2:
            last_row = self.img[last_idx, :]
            lane_exists = np.where(last_row > 0)[0] # lane exists where there is a nonzero value
            if len(lane_exists) > 0: # If lane exists
                lane_center = np.median(lane_exists)
                center_point = (int(lane_center), (self.img_shape_0 + last_idx))
                return center_point
            # Else move up by 1 pixel
            last_idx -= 1       # Removed BUG here
        return None


# Thresholding operations

def thresh_image(img, range):
    """
    img: grayscale image
    range: tuple of (min, max) threshold values
    """
    return cv2.inRange(img, range[0], range[1])

def combine_images(images):
    """
    images: list of images to combine
    """
    if len(images) == 1:
        return images[0]
    else:
        return cv2.bitwise_and(*images)

# Morphological Operations

def erode(image, kernel_size=5, iterations=1):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))
    erosion = cv2.erode(image, kernel, iterations=iterations)
    return erosion

def dilate(image, kernel_size=5, iterations=1):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))
    dilation = cv2.dilate(image, kernel, iterations=iterations)
    return dilation

def open_op(image, kernel_size=5, iterations=1):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))
    opening = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel, iterations=iterations)
    return opening

def close_op(image, kernel_size=5, iterations=1):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))
    closing = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel, iterations=iterations)
    return closing

# Other Operations

def get_largest_contour(img):
    """
    Get the largest contour from the image
    """
    contours, _ = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea, reverse=True)

    img = np.zeros_like(img)
    cv2.drawContours(img, [contours[0]], -1, 255, -1)
    return img

def gauss_blur(img, kernel_size=3):
    """
    Apply a Gaussian Blur to the image
    """
    return cv2.GaussianBlur(img, (kernel_size, kernel_size), 0)

# Thinning

def thin_op(img):
    """
    Apply Thinning to the image
    """
    return cv2.ximgproc.thinning(img)

# Manual Search
# TODO: Optimization
def manual_search(image, step = 50):
    # Get points on the image
    points = np.argwhere(image > 0)
    # Sort the points by x
    points = points[points[:,0].argsort()]
    y = points[-1][0]
    x_values = []
    y_values = []
    x_step = None
    while y > 0:
        x = points[points[:,0] == y][:,1]
        x = x[len(x)//2]    # median
        x_values.append(x)
        y_values.append(y)
        y -= step
        # Calculate angle between most recent points
        if len(y_values) > 2:
            angle = np.arctan2(x_values[-1] - x_values[-2], y_values[-1] - y_values[-2])
            angle = np.degrees(angle)
            if angle > 0 and angle < 135:   # The line is moving right
                x_step = step
                break
            elif angle < 0 and angle > -135:    # The line is moving left
                x_step = -step
                break
    
    if x_step:
        while x > 0 and x < image.shape[1]:
            try:
                y = points[points[:,1] == x][:,0]
                y = y[len(y)//2]    # median
                x_values.append(x)
                y_values.append(y)
                x += x_step
            except:
                break
    
    points = list(zip(x_values, y_values))
    return points

def plot_points(image, points):
    # convert img to color
    img = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
    # create an image with the same shape as img
    #img = np.zeros_like(img)
    for point in points:
        cv2.circle(img, point, 5, (255, 0, 0), -1)
    return img

class ImageProcessor:
    def __init__(self, config):
        self.get_params_from_config(config)
    
    def load_image(self, image):
        """Converts the image to HLS and returns the HLS channels"""
        hls = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
        self.h, self.l, self.s = cv2.split(hls)
    
    def get_params_from_config(self, config):
        """Extracts the params fromt he config dict"""
        self.hls_select = config.get("hls_select", [])
        self.h_range = config.get("h_range", None)
        self.l_range = config.get("l_range", None)
        self.s_range = config.get("s_range", None)
        self.morph = config.get("morph", False)
        self.erosion = config.get("erosion", False)
        self.dilation = config.get("dilation", False)
        self.opening = config.get("opening", False)
        self.closing = config.get("closing", False)
        self.kernel_erode = config.get("kernel_erode", None)
        self.kernel_dilate = config.get("kernel_dilate", None)
        self.kernel_open = config.get("kernel_open", None)
        self.kernel_close = config.get("kernel_close", None)
        self.largcon = config.get("largcon", False)
        self.blur = config.get("blur", False)
        self.kernel_blur = config.get("kernel_blur", None)
        self.shape = config.get("shape", None)
        self.radius = config.get("radius", None)
        self.start_angle = config.get("start_angle", None)
        self.end_angle = config.get("end_angle", None)
        self.a = config.get("a", None)
        self.b = config.get("b", None)
        self.draw = config.get("draw", False)
    
    def threshold_image(self):
        thresholded = []
        if not len(self.hls_select) == 0:
            for channel in self.hls_select:
                if channel == "Hue":
                    thresholded.append(thresh_image(self.h, self.h_range))
                elif channel == "Lightness":
                    thresholded.append(thresh_image(self.l, self.l_range))
                elif channel == "Saturation":
                    thresholded.append(thresh_image(self.s, self.s_range))

        self.im = combine_images(thresholded)
        return self.im

    def preprocess_image(self):
        """Applies pre-processing operations on the image such as - 
        - Morphological operations (optional)
        - Get Largest contour (optional)
        - Blur (optional)
        """
        # Morphological operations
        if self.morph:
            if self.erosion:
                self.im = erode(self.im, self.kernel_erode)
            if self.dilation:
                self.im = dilate(self.im, self.kernel_dilate)
            if self.opening:
                self.im = open_op(self.im, self.kernel_open)
            if self.closing:
                self.im = close_op(self.im, self.kernel_close)
        
        # Get largest contour
        if self.largcon:
            self.im = get_largest_contour(self.im)

        # Blur operation        
        if self.blur:
            self.im = gauss_blur(self.im, self.kernel_blur)

        return self.im

    def detect_line(self):
        """Detects the line using either of the specified methods"""
        if self.shape == "Circle":
            ld = LineDetectorCircle(self.radius, self.start_angle, self.end_angle, draw = self.draw)
        elif self.shape == "Ellipse":
            ld = LineDetectorEllipse(self.a, self.b, self.start_angle, self.end_angle, draw = self.draw)
        points = ld(self.im)
        img_det = ld.img_color if self.draw else None
        return points, img_det
    
    def process(self, image):
        """Processes the image and returns the detected line"""
        self.load_image(image)
        self.threshold_image()
        self.preprocess_image()
        points, img_det = self.detect_line()
        return points, img_det
    

# Save config to a file
def save_config(config, file_path):
    with open(file_path, 'w') as f:
        yaml.dump(config, f)

# Load the config from a file
def load_config(file_path):
    with open(file_path, 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    return convert_bool_strings(config)

# Convert boolean strings back to boolean values
def convert_bool_strings(obj):
    if isinstance(obj, dict):
        return {k: convert_bool_strings(v) for k, v in obj.items()}
    elif isinstance(obj, list):
        return [convert_bool_strings(item) for item in obj]
    elif isinstance(obj, str):
        if obj.lower() == 'true':
            return True
        elif obj.lower() == 'false':
            return False
        else:
            return obj
    else:
        return obj